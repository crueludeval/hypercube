SOURCE = nl/harmwal/hypercube/Space.java
SOURCES = nl/harmwal/hypercube/Version.java \
	nl/harmwal/hypercube/Build.java \
	nl/harmwal/hypercube/Global.java \
	nl/harmwal/hypercube/Main.java \
	nl/harmwal/hypercube/Launcher.java \
	$(SOURCE) \

CLASSES = nl/harmwal/hypercube/Version.class \
	nl/harmwal/hypercube/Build.class \
	nl/harmwal/hypercube/Global.class \
	nl/harmwal/hypercube/Main.class \
	nl/harmwal/hypercube/Launcher.class \
	nl/harmwal/hypercube/Space.class

WAV=sounds/activate.wav sounds/clicked.wav sounds/email.wav sounds/info.wav sounds/shutdown1.wav sounds/toggled.wav sounds/warning.wav
AU=sounds/activate.au sounds/clicked.au sounds/email.au sounds/info.au sounds/shutdown1.au sounds/toggled.au sounds/warning.au

HTMLSOURCE = nl/harmwal/hypercube/Space.java.html
JAR = hypercube.jar

all: $(HTMLSOURCE) $(JAR)

$(HTMLSOURCE): $(SOURCE)
	java2html $(SOURCE)

$(JAR): $(CLASSES) README Manifest.txt sounds/README
	jar -cfm $(JAR) Manifest.txt  README nl/harmwal/hypercube/*.class sounds/*.au sounds/README

$(CLASSES): $(SOURCES) Makefile
	echo "package nl.harmwal.hypercube; class Build {static final String date = " \"`date +%Y%m%d%H%M`\"";}" >nl/harmwal/hypercube/Build.java
	javac -g -deprecation -target 1.1 $(SOURCES)

$(AU): $(WAV)

run: $(JAR)
	java -jar $(JAR) 4 "cross eyed" false false

clean:
	rm -f nl/harmwal/hypercube/*.class core*

.SUFFIXES: .wav .au

.wav.au:
	sox $< -r 8012 -U -b $@

